import * as React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { 
  createDrawerNavigator
} from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';

import HomeScreens from '../Home'
import CustomDrawerContent from '../../component/Drawer'
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout';

function Halo() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Halooo!</Text>
    </View>
  );
}


const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function Draw() {
  return (
      <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
        <Drawer.Screen name="Drawers" component={HomeScreens} />
      </Drawer.Navigator>
  );
}

export default function App() {
    return (
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = focused ? 'home' : 'home';
              } else if (route.name === 'Like') {
                iconName = focused ? 'heart' : 'heart';
              }else if (route.name === 'Person') {
                iconName = focused ? 'person' : 'person';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
            showLabel: false,
            style:{backgroundColor: '#f2f2f2'}
          }}
        >
          <Tab.Screen name="Home" component={Draw} />
          <Tab.Screen name="Like" component={Halo} />
          <Tab.Screen name="Person" component={Halo} />
        </Tab.Navigator>
    );
  }