import React, { useState } from 'react';
import {SafeAreaView, View, Text, TouchableOpacity, ScrollView, TextInput, Image} from 'react-native';
import styles from './style'

import Header from '../../component/Header'
import Banner from '../../component/Banner'
import Product from '../../component/Product'

function Home(props) {
    const [count, setCount] = useState(0)

    function cardCount(params) {
        setCount(count+params)
    }

    return(
        <SafeAreaView style={{...styles.container}} >
            <View style={{...styles.backView}}>
                <Header 
                    {...props}
                    bridge={count}
                />
                <ScrollView>   
                    <Text style={{...styles.textTitle}}>Nike App Store</Text>
                    <Banner />
                    <Product 
                        result={(res)=> cardCount(res)}
                    />
                </ScrollView>
            </View>
        </SafeAreaView>
    )

}

export default Home;