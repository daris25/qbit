import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        width: '100%', 
        height: '100%', 
        backgroundColor: 'white'
    },
    backView:{
        backgroundColor: '#f2f2f2', 
        height: '100%', 
        width: '100%', 
        borderTopRightRadius: 200
    },
    textTitle:{
        fontSize: 15,
        color: 'black',
        fontWeight: 'bold',
        marginLeft: 15,
        marginTop: 15,
        marginBottom: 15
    }
})