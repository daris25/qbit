import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 20,
        marginTop: 10,
        // marginBottom: 20,
        // backgroundColor: 'grey'
    },
    bridge:{
        height: 15,
        width: 15,
        backgroundColor: 'pink',
        borderRadius: 100,
        position: 'absolute',
        right: 8,
        top: -2,
        justifyContent: 'center',
        alignItems: 'center'
    },
        textBridge:{
            fontSize: 10,
            fontWeight: 'bold',
            color: 'white'
        }
})