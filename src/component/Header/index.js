import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './style'
import Feather from 'react-native-vector-icons/Feather'

function Header(props) {
    console.log('props home', props.navigation)

    const sideMenu = ()=>{
        // props.navigation.toggleDrawer()
        const cek = props.navigation.openDrawer('1')
        console.log('cek', cek)
    }

    return(
        <View style={{...styles.container}}>
            <TouchableOpacity
                onPress={sideMenu}
            >
                <Feather name="menu" size={22} color="black" />
            </TouchableOpacity>
            <TouchableOpacity>
                <Feather name="shopping-cart" size={22} color="black" />
            </TouchableOpacity>

            {
                props.bridge == 0 ? 
                    null
                : 
                    <View style={{...styles.bridge}}>
                        <Text style={{...styles.textBridge}}>{props.bridge}</Text>
                    </View>
            }
        </View>
    )
}

export default Header;