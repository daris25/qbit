import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    cardProduct:{
        width: 130,
        height: 200,
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 3,
        marginTop: 5,
        marginBottom: 5,
        justifyContent: 'space-between',
    },
        imgCard:{
            width: '100%',
            height: '80%',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            resizeMode: 'stretch'
        },
        addCard:{
            width: '100%',
            height: '20%',
            backgroundColor: 'grey',
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 10,
            paddingRight: 10
        },
        textCard:{
            fontSize: 12,
            color: 'white',
            marginLeft: 5
        }
})