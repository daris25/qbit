import React from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, ToastAndroid} from 'react-native';
import styles from './style'
import AntDesign from 'react-native-vector-icons/AntDesign'

function Product(props) {

    function pressOut(params) {
        // console.log('prs one')
        props.result(1)
        ToastAndroid.show(params.name+' Added', ToastAndroid.LONG);
    }   

    return(
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {
                json.map((data)=>{
                    return(
                        <View 
                            key={data.id}
                            style={{...styles.cardProduct, marginLeft: data.id == 0 ? 15 : 5, marginRight: data.id == 2 ? 15 : 5}}
                        >   
                            <Image 
                                // resizeMethod="scale"
                                source={data.image}
                                style={{...styles.imgCard}}
                            />
                            <View
                                style={{...styles.addCard}}
                            >
                               <TouchableOpacity
                                    onPress={()=> pressOut(data)}
                               >
                                    <AntDesign name="plussquare" size={20} color="white" />
                               </TouchableOpacity>
                               <Text style={{...styles.textCard}}>{data.name}</Text>
                            </View>
                        </View>
                    )
                })
            }
        </ScrollView>
    )

}

export default Product;

const json = 
[
    {
        id: 0,
        image: require('../../assets/images/spt1.jpeg'),
        name: 'Nike Vapormax'
    },
    {
        id: 1,
        image: require('../../assets/images/spt2.jpeg'),
        name: 'Nike Airmax'
    },
    {
        id: 2,
        image: require('../../assets/images/spt3.jpeg'),
        name: 'Nike Lunar'
    }
]