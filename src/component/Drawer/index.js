import * as React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { 
  DrawerContentScrollView
} from '@react-navigation/drawer';
import AntDesign from 'react-native-vector-icons/AntDesign'

export default function CustomDrawerContent(props) {
    return (
      <DrawerContentScrollView 
        {...props}
        contentContainerStyle={{justifyContent: 'space-between', width: '100%', height:'100%', paddingLeft: 15, paddingRight: 15}}
      >
            <View>
                <TouchableOpacity
                    onPress={()=> console.log('notig')}
                    style={{width: '100%', height : 40, flexDirection: 'row', alignItems: 'center'}}
                >
                    <AntDesign name="notification" size={30} />
                    <Text style={{fontSize: 15, marginLeft: 15}}>Notification</Text>
                </TouchableOpacity>
            </View>
      </DrawerContentScrollView>
    );
  }