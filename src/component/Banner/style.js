import { Dimensions, StyleSheet } from 'react-native';

const width = Dimensions.get('screen').width
const fixWidth = width-(width/100*10-20)

export default StyleSheet.create({
    viewCard:{
        width: width,
        height: 120,
        // backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backImage:{
        width: '90%',
        height: '95%',
        // backgroundColor: 'grey',
        borderRadius: 10,
    },

    imgCard:{
        width: '100%', 
        height: '100%', 
        borderRadius: 10,
        // elevation: 3
    },
    viewDot:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 40,
        flexDirection: 'row'
    },
        dotImg:{
            width: 9,
            height: 9,
            borderRadius: 100,
            borderWidth: 1,
            borderColor: 'black',
            marginRight: 3
        }

})