import React, { useState } from 'react';
import {View, Text, TouchableOpacity, ScrollView, Image, Dimensions} from 'react-native';
import styles from './style'


function Banner(props) {
    const width = Dimensions.get('window').width
    const [currentPage, setCurrentPage] = useState(0)

    const elementScrollData = (event) => {
        console.log((event.nativeEvent.contentOffset.x / width))
        const dots = event.nativeEvent.contentOffset.x / width
        setCurrentPage(dots)
    }
    console.log('ini layer', currentPage);

    return(
        <View>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                bounces={false}
                showsHorizontalScrollIndicator={false}
                onScroll={elementScrollData}
                disableIntervalMomentum={true}
                onContentSizeChange={(w,h)=> console.log(w,h)}
                scrollEventThrottle={0}
            >  
                {
                    json.map((data)=>{
                        return(
                            <View key={data.id} style={{...styles.viewCard}}>
                                <View style={{...styles.backImage}}>
                                    <Image
                                        source={data.image}
                                        style={{...styles.imgCard}}
                                    />
                                </View>
                            </View>
                        )
                    })
                }
            </ScrollView>
            <View style={{...styles.viewDot}}>
                {
                    json.map((dot)=>{
                        return(
                            <View 
                                style={{...styles.dotImg, backgroundColor: currentPage == dot.id ? 'black': null}} 
                                key={dot.id}
                            />
                        )
                    })
                }
            </View>
        </View>
    )

}

export default Banner;

const json = 
[
    {
        "id" : "0",
        "image": require('../../assets/images/banner1.jpg')
    },
    {
        "id" : "1",
        "image": require('../../assets/images/banner2.jpeg')
    },
    {
        "id" : "2",
        "image": require('../../assets/images/banner3.jpg')
    }
]